from django.urls import path,include
from . import views

app_name = 'naufal_profile'

urlpatterns = [
    path('', views.index, name='index'),
    path('porto', views.porto, name='porto'),
    path('jadwal', include('jadwal.urls'), name='jadwal'),
]