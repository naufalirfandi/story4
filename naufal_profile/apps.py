from django.apps import AppConfig


class NaufalProfileConfig(AppConfig):
    name = 'naufal_profile'
