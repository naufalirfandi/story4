from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'index.html')

def porto(request):
    return render(request, 'porto.html')