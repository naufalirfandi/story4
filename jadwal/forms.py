from django import forms

class formulir(forms.Form):
    nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Kegiatan',
        'type' : 'text',
        'required' : True
    }))
    waktu_kegiatan = forms.CharField(widget=forms.DateTimeInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Tanggal dan Waktu',
        'type' : 'datetime-local',
        'required' : True
    }))
    tempat_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Tempat Kegiatan',
        'type' : 'text',
        'required' : True
    }))
    kategori_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Kategori Kegiatan',
        'type' : 'text',
        'required' : True
    }))
    hari_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Hari',
        'type' : 'text',
        'required' : True
    }))
