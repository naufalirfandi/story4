from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.
def jadwal(request):
    if(request.method == "POST"):
        tmp = forms.formulir(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.Jadwal()
            tmp2.nama_kegiatan = tmp.cleaned_data["nama_kegiatan"]
            tmp2.hari_kegiatan = tmp.cleaned_data["hari_kegiatan"]
            tmp2.tempat_kegiatan = tmp.cleaned_data["tempat_kegiatan"]
            tmp2.kategori_kegiatan = tmp.cleaned_data["kategori_kegiatan"]
            tmp2.waktu_kegiatan = tmp.cleaned_data["waktu_kegiatan"]
            tmp2.save()
        return redirect("/jadwal")
    else:
        tmp = forms.formulir()
        tmp2 = models.Jadwal.objects.all()
        tmp_dictio = {
            'formulir' : tmp,
            'jadwal' : tmp2
        }
        return render(request, 'jadwal/jadwal.html', tmp_dictio)

def delete(request, pk):
    if(request.method == "POST"):
        tmp = forms.formulir(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.Jadwal()
            tmp2.nama_kegiatan = tmp.cleaned_data["nama_kegiatan"]
            tmp2.hari_kegiatan = tmp.cleaned_data["hari_kegiatan"]
            tmp2.tempat_kegiatan = tmp.cleaned_data["tempat_kegiatan"]
            tmp2.kategori_kegiatan = tmp.cleaned_data["kategori_kegiatan"]
            tmp2.waktu_kegiatan = tmp.cleaned_data["waktu_kegiatan"]
            tmp2.save()
        return redirect("/jadwal")
    else:
        models.Jadwal.objects.filter(pk = pk).delete()
        tmp = forms.formulir()
        tmp2 = models.Jadwal.objects.all()
        tmp_dictio = {
            'formulir' : tmp,
            'jadwal' : tmp2
        }
        return render(request, 'jadwal/jadwal.html', tmp_dictio)

