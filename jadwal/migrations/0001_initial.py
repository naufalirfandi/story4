# Generated by Django 2.2.6 on 2019-10-07 05:09

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_kegiatan', models.CharField(max_length=100)),
                ('hari_kegiatan', models.CharField(max_length=100)),
                ('tempat_kegitan', models.CharField(max_length=100)),
                ('kategori_kegiatan', models.CharField(max_length=100)),
                ('waktu_kegiatan', models.DateTimeField()),
            ],
        ),
    ]
