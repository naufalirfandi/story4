from django.db import models

# Create your models here.
class Jadwal(models.Model):
    nama_kegiatan = models.CharField(blank=False, max_length= 100)
    hari_kegiatan = models.CharField(blank=False, max_length= 100)
    tempat_kegiatan = models.CharField(blank=False, max_length= 100)
    kategori_kegiatan = models.CharField(blank=False, max_length= 100)
    waktu_kegiatan = models.DateTimeField(blank=False)