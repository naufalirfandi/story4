from django.urls import path
from . import views

urlpatterns = [
    path('', views.jadwal),
    path('<int:pk>/', views.delete, name = 'hapus'),
]